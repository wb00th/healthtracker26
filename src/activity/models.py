from django.db import models
from django.urls import reverse
from datetime import date
# Create your models here.


class Activity(models.Model):
    title = models.CharField(max_length=100)
    details = models.TextField(blank=True, null=True)
    duration = models.DurationField(blank=True, null=True)
    dateDone = models.DateField(blank=True, default=date.today())

    def get_absolute_url(self):
        return reverse("activity:activity details", kwargs={"id": self.id})
