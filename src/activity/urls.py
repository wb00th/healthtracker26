from django.urls import path
from .views import (
    ActivityCreateView,
    ActivityDeleteView,
    ActivityDetailView,
    ActivityListView,
    ActivityUpdateView
)

app_name = 'activity'
urlpatterns = [
    path('create', ActivityCreateView, name='activity create'),
    path('<int:id>/', ActivityDetailView.as_view(), name='activity details'),
    path('<int:id>/update/', ActivityUpdateView, name='activity update'),
    path('', ActivityListView.as_view(), name='activity list'),
    path('<int:id>/delete/', ActivityDeleteView.as_view(), name='activity delete'),
    ]