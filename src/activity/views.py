from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views.generic import(
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    DeleteView
)
from .forms import ActivityForm
from .models import Activity
# Create your views here.


def ActivityCreateView(request):
    form = ActivityForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = ActivityForm()
    context = {
        'form': form,
    }
    return render(request, "activity/create.html", context)

class ActivityListView(ListView):
    template_name = 'activity/list.html'
    queryset = Activity.objects.all()


class ActivityDetailView(DetailView):
    template_name = 'activity/detail.html'
    queryset = Activity.objects.all()

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Activity, id=id_)


def ActivityUpdateView(request, id=id):
    obj = get_object_or_404(Activity, id=id)
    form = ActivityForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
    context = {
        'form': form
    }
    return render(request, "activity/create.html", context)

class ActivityDeleteView(DeleteView):
    template_name = 'activity/delete.html'
    queryset = Activity.objects.all()

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Activity, id=id_)

    def get_success_url(self):
        return reverse("activity:activity list")
