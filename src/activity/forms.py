from django import forms
from django.forms import widgets
from .models import Activity
import datetime
from datetime import date


class ActivityForm(forms.ModelForm):
    title = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "Title"}))
    details = forms.CharField(
        required=False,
        widget=forms.Textarea(
            attrs={"placeholder": "Details"}))
    duration = forms.DurationField(
        required=False,
    )
    dateDone = forms.DateField(
        label='Date Activity Was Done',
        widget=widgets.SelectDateWidget())

    class Meta:
        model = Activity
        fields = [
            'title',
            'details',
            'duration',
            'dateDone',
        ]

    def clean_dateDone(self, *args, **kwargs):
        datedone = self.cleaned_data.get("dateDone")
        if not datedone < date.today()+datetime.timedelta(days=1):
            raise forms.ValidationError("Date Cannot Be In The Future")
        return datedone
