from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.forms import widgets
from datetime import date
from .models import Profile

class UserRegisterForm(UserCreationForm):
    username = forms.CharField(max_length=30)
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    date_of_birth = forms.DateField(label='date_of_birth', widget=widgets.SelectDateWidget(years=range(1900, 2019)))
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
    	model = User
    	fields = ['username', 'first_name', 'last_name', 'email', 'date_of_birth', 'password1', 'password2']

class UserUpdateForm(forms.ModelForm):
    username = forms.CharField(max_length=30, required=False)
    last_name = forms.CharField(max_length=30, required=False)
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.', required=False)
    date_of_birth = forms.DateField(label='date_of_birth', widget=widgets.SelectDateWidget(years=range(1900, 2019)))

    class Meta:
    	model = User
    	fields = ['username', 'last_name', 'email']

class ProfileUpdateForm(forms.ModelForm):
	class Meta:
		model = Profile
		fields = ['image']