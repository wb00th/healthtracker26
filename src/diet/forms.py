from django import forms
from django.forms import widgets
from .models import Meal, Plan, Food
from datetime import date


class MealForm(forms.ModelForm):
    meal = forms.ChoiceField()

    class Meta:
        model = Meal
        fields = [
            'title',
            'description',
            'targetDate',
        ]

    def clean_targetDate(self, *args, **kwargs):
        targetdate = self.cleaned_data.get("targetDate")
        if not targetdate > date.today():
            raise forms.ValidationError("Target Date Cannot Be Before Today")
        return targetdate
