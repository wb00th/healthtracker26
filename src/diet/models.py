from django.db import models
from django.urls import reverse

# Create your models here.


class Meal(models.Model):
    BREAKFAST = 'BR'
    LUNCH = 'LU'
    DINNER = 'DI'
    MEAL_CHOICES = [
        (BREAKFAST, 'Breakfast'),
        (LUNCH, 'Lunch'),
        (DINNER, 'Dinner')
    ]
    meal = models.CharField(blank=False, max_length=2,
                            choices=MEAL_CHOICES, default=BREAKFAST)
    food = models.ForeignKey('Food', to_field='name', on_delete=models.PROTECT)
    date = models.DateField(auto_now=True)

    def get_absolute_url(self):
        return reverse("diet:diet details", kwargs={"id": self.id})


class Plan(models.Model):
    HIGHCAL = 'HC'
    LOWCAL = 'LC'
    NORMAL = 'NO'
    DIET_TYPE = [
        (HIGHCAL, 'High Calorie'),
        (LOWCAL, 'Low Calorie'),
        (NORMAL, 'Normal')
    ]
    plan = models.CharField(blank=False, max_length=2,
                            choices=DIET_TYPE, default=NORMAL)
    days = models.IntegerField()
    maxCal = models.IntegerField()
    minCal = models.IntegerField()
    consumed = models.IntegerField(default=0)


class Food(models.Model):
    name = models.CharField(primary_key=True, max_length=200)
    calories = models.IntegerField()
    recipe = models.TextField(blank=True, null=True)
