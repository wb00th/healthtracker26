from django.shortcuts import render

# Create your views here.

def home_view(request, *args, **kwargs):
    home_context = {
        "title": "Welcome To Apex Health Tracker",
    }
    return render(request, "home.html", home_context)

def login_view(request, *args, **kwargs):
    login_context = {
        "title": "HealthTracker - Login",
    }
    return render(request, "login.html", login_context)

def register_view(request, *args, **kwargs):
    register_context = {
        "title": "HealthTracker - Register",
    }
    return render(request, "register.html", register_context)

def user_view(request, *args, **kwargs):
    user_context = {
        "title": "User",
    }
    return render(request, "user.html", user_context)

def group_view(request, *args, **kwargs):
    group_context = {
        "title": "Group",
        "header": "Your Groups"
    }
    return render(request, "group.html", group_context)

def goals_view(request, *args, **kwargs):
    goals_context = {
        "title": "Goals",
        "header": "Your Goals"
    }
    return render(request, "goals.html", goals_context)

def exercise_view(request, *args, **kwargs):
    exercise_context = {
        "title": "Exercise",
        "header": "Your Exercise Regimen"
    }
    return render(request, "exercise.html", exercise_context)

def diet_view(request, *args, **kwargs):
    diet_context = {
        "title": "Diet",
        "header": "Your Diet"
    }
    return render(request, "diet.html", diet_context)

def account_view(request, *args, **kwargs):
    account_context = {
        "title": "Account Settings",
        "header": "Account Settings"
    }
    return render(request, "account.html", account_context)