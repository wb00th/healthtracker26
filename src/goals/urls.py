from django.urls import path
from .views import (
    GoalsDetailView,
    GoalsCreateView,
    GoalsUpdateView,
    GoalsDeleteView,
    GoalsListView,
)

app_name = 'goals'
urlpatterns = [
    path('create', GoalsCreateView, name='goal create'),
    path('<int:id>/', GoalsDetailView.as_view(), name='goal details'),
    path('<int:id>/update', GoalsUpdateView.as_view(), name='activity update'),
    path('<int:id>/delete', GoalsDeleteView.as_view(), name='goal delete'),
    path('', GoalsListView.as_view(), name='goal list'),
]