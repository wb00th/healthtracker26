from django import forms
from django.forms import widgets
from .models import Goal
from datetime import date


class GoalCreationForm(forms.ModelForm):
    title = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "Title"}))
    description = forms.CharField(
        required=False,
        widget=forms.Textarea(
            attrs={"placeholder": "Description"}))
    targetDate = forms.DateField(
        label='Target Date',
        widget=widgets.SelectDateWidget())

    class Meta:
        model = Goal
        fields = [
            'title',
            'description',
            'targetDate',
        ]

    def clean_targetDate(self, *args, **kwargs):
        targetdate = self.cleaned_data.get("targetDate")
        if not targetdate > date.today():
            raise forms.ValidationError("Target Date Cannot Be Before Today")
        return targetdate


class GoalUpdateForm(forms.ModelForm):
    complete = forms.BooleanField(label='Goal Complete?', required=False)

    class Meta:
        model = Goal
        fields = [
            'complete'
        ]