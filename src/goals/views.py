from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from .forms import GoalUpdateForm, GoalCreationForm
from .models import Goal

from django.views.generic import(
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    DeleteView
)
# Create your views here.


def GoalsCreateView(request):
    form = GoalCreationForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = GoalCreationForm()
    context = {
        'form': form,
    }
    return render(request, "goals/create.html", context)

class GoalsListView(ListView):
    template_name = 'goals/list.html'
    queryset = Goal.objects.all()


class GoalsDetailView(DetailView):
    template_name = 'goals/detail.html'
    queryset = Goal.objects.all()

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Goal, id=id_)


class GoalsUpdateView(UpdateView):
    template_name = 'goals/create.html'
    form_class = GoalUpdateForm
    queryset = Goal.objects.all()

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Goal, id=id_)

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)


class GoalsDeleteView(DeleteView):
    template_name = 'goals/delete.html'
    queryset = Goal.objects.all()

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Goal, id=id_)

    def get_success_url(self):
        return reverse("goals:goals list")
