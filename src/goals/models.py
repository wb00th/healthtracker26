from django.db import models
from django.urls import reverse
from datetime import date
# Create your models here.


class Goal(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    dateSet = models.DateField(auto_now=True)
    targetDate = models.DateField()
    complete = models.BooleanField(default=False)

    def is_past_target(self):
        return date.today() > self.targetDate

    def get_absolute_url(self):
        return reverse("goals:goal details", kwargs={"id": self.id})
